export default {
    // Target (https://go.nuxtjs.dev/config-target)
    target: 'server',
    // server: {
    //     port: 8000,   
    //     host: '0.0.0.0', 
    // },

    // Global page headers (https://go.nuxtjs.dev/config-head)
    // mode: 'universal',
    head: {
        title: 'Sketsahouse Digital Agency',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ],
        script: [
            { src: '/vendor/bootstrap/bootstrap.bundle.min.js' },
            { src: '/vendor/popper/popper.min.js' },
        ]
    },

    // Global CSS (https://go.nuxtjs.dev/config-css)
    css: [
        'bootstrap/dist/css/bootstrap.min.css',
        'bootstrap-icons/font/bootstrap-icons.css',
        '@/static/css/style.css',
        '@/static/css/fonts.css',
        'aos/dist/aos.css'
    ],

    // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
    plugins: [
        { src: '~/plugins/vue-awesome-swiper' },
        { src: '~/plugins/aos', ssr: false },
        { src: '~/plugins/slugify' },
        { src: '~/plugins/v-snip', ssr: false },

    ],

    //Loading
    // loading: '~/components/Widget/Loading.vue',


    // Auto import components (https://go.nuxtjs.dev/config-components)
    components: true,

    // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
    buildModules: [],

    // Modules (https://go.nuxtjs.dev/config-modules)
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        // https://go.nuxtjs.dev/content
        // '@nuxt/content',
        // https://http.nuxtjs.org/
        // '@nuxt/http',
        // https://prismic.nuxtjs.org/
        // '@nuxtjs/prismic',

    ],

    // Axios module configuration (https://go.nuxtjs.dev/config-axios)
    axios: {
        baseURL: process.env.BASE_API,
        common: {
            'Content-Type': 'application/json',
        },
    },

    // Content module configuration (https://go.nuxtjs.dev/config-content)
    content: {},

    // Build Configuration (https://go.nuxtjs.dev/config-build)
    build: {
        extractCSS: true,
        // or
        extractCSS: {
            ignoreOrder: true
        }
    },


}