import AOS from 'aos/dist/aos'
export default ({ app }, inject) => {
    app.AOS = new AOS.init();
}